# -*- coding: utf8 -*-
import numpy as np
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class ScatterAnim(object):
    def __init__(self, N, L, path_base):
        self.data_path = path_base + "/%i.out"

        self.N = N  # the number of the agents in the system
        self.L = L  # the size of the system

        # set the figure
        self.fig = plt.figure(figsize=(16, 8))

        # set the fps
        self.fps = 20

        # for the scatter
        self.scatter_ax = plt.subplot2grid((2, 4), (0, 0), colspan=2, rowspan=2)
        self.scatter_ax.set_xlim([0, self.L])
        self.scatter_ax.set_ylim([0, self.L])
        self.scatter_ax.set_xlabel('Simulation')

        # for average velocity
        self.v_x_s = np.array([])
        self.v_y_s = np.array([])
        self.aver_v_ax = plt.subplot2grid((2, 4), (0, 2), colspan=2, rowspan=1)
        self.aver_v_ax.set_xlim([0, 10])
        self.aver_v_ax.set_ylim([0, 1.1])
        self.aver_v_ax.set_xlabel('Time', labelpad=-15)
        self.aver_v_ax.set_ylabel('$\phi$', fontsize=20)
        self.aver_v_line, = self.aver_v_ax.plot([], [])

        # for center mass
        self.center_mass_x_s = np.array([])
        self.center_mass_y_s = np.array([])
        self.center_mass_ax = plt.subplot2grid((2, 4), (1, 2), colspan=2, rowspan=1)
        self.center_mass_ax.set_xlim([0, 10])
        self.center_mass_ax.set_ylim([0, 10])
        self.center_mass_line, = self.center_mass_ax.plot([], [])

        #To do the animation
        self.ani1 = animation.FuncAnimation(
            self.fig,
            self.update_scatt,
            interval=self.fps,
            #init_func=self.setup_plot,
            blit=True
        )

    # The core part of the simulation.
    def update_scatt(self, k):
        print k

        # open the data file
        #filename = "./data/%i.out" % k
        filename = self.data_path % k

        frame = np.loadtxt(filename).T

        # calculate the sum velocity of all agents
        self.sumx = np.average(frame[2, :])
        self.sumy = np.average(frame[3, :])

        # append the new value to the line
        self.v_x_s = np.append(self.v_x_s, k*0.01)
        self.v_y_s = np.append(self.v_y_s, np.sqrt((self.sumx)**2+(self.sumy)**2))

        # adjust the ax limit when exceeded
        self.xmin, self.xmax = self.aver_v_ax.get_xlim()
        if k*0.01 > self.xmax:
            self.aver_v_ax.set_xlim(self.xmin, 2*self.xmax)

        # calculate the center mass
        self.center_mass_x_s = np.append(self.center_mass_x_s, k*0.01)
        self.center_mass_y_s = np.append(self.center_mass_y_s, np.average(frame[1, :]))

        # draw the ax
        self.scat = self.scatter_ax.scatter(frame[0, :], frame[1, :], animated=True)
        self.aver_v_line.set_data(self.v_x_s, self.v_y_s)
        self.center_mass_line.set_data(self.center_mass_x_s, self.center_mass_y_s)

        return self.scat, self.aver_v_line, self.center_mass_line


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 1:
        file_base = "./data"
    else:
        file_base = "./demo/" + sys.argv[1]
    with open("./config/strombom.cfg") as f:
        N = int(f.readline().split("=")[-1])
        L = int(f.readline().split("=")[-1])
    a = ScatterAnim(N, L, file_base)
    plt.show()
