#ifndef MODELS_H
#define MODELS_H

#include "collective.h"
#include "rand.h"
#include "utils.h"
#include <cmath>

using namespace std;

/* Base model for all the vicsek variants.
 * there are several assuptions:
 *     1. all the agents move on a square plane with periodic boundry
 *     2. they share the same way of being perturbated
 *
 * All the derived classes have to implement updateDirections
 * method
 *
 * The template parameter Agent has to base on collective::TwoDAgent
 *     */
template <typename Agent, typename Collection>
class BaseModelR
{
    public:
//        typedef collective::CollectionPeriodicRBF<Agent> Collection;

        int _num; // the number of agents
        double _v; // the velocity factor of every agent
        int _length; // the length of the field
        double _eta; // the amplitude of the perturbation
        Collection colle ; // the collection to store all the info
        mylib::RandDouble rd_gen; // random number generator
        int times; // the times the simulation has been run

        /* Constructors */
        BaseModelR(int num, double v, int length, double eta, double radius): _num(num), _v(v), _length(length), _eta(eta), colle(num, length, radius), rd_gen(), times(0)
        {
            colle.shufflePosition();
            colle.shuffleDirection();
        }

        /* One time step of simulation */
        virtual BaseModelR & oneTimeStep() = 0;
        /* Movie the agent and calculate the direction */
        virtual Agent & updateAgentsDirection(int index) = 0;
        /* Perturbate the agent */
        virtual Agent & perturbateAgent(int index)
        {
            Agent & the_agent = colle[index];

            double x(the_agent.velocity.getX());
            double y(the_agent.velocity.getY());

            double theta(rd_gen.gen() * _eta - _eta / 2.0);
            double cos_theta(cos(theta));
            double sin_theta(sin(theta));

            the_agent.velocity.setX(x * cos_theta + y * sin_theta);
            the_agent.velocity.setY(y * cos_theta - x * sin_theta);

            return the_agent;
        }
        /* IO methods */
        virtual BaseModelR & dumpPosition(ostream & out)
        {
            for(int i=0; i<_num; ++i)
            {
                out << colle[i].position.getX()
                    << " "
                    << colle[i].position.getY()
                    << " "
                    << colle[i].velocity.getX()
                    << " "
                    << colle[i].velocity.getY()
                    << std::endl;
            }
            return *this;
        }

};

#endif
