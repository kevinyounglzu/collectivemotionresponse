#ifndef STROMBOM2_H
#define STROMBOM2_H

#include "collective.h"
#include "models.h"

/* This means to be a totally new variation of strombom model */
class Strombom2Base: public BaseModelR<collective::TwoDAgent, collective::CollectionReflectionRBF<collective::TwoDAgent>>
{
    public:
        typedef collective::TwoDAgent Agent;
        typedef collective::CollectionReflectionRBF<collective::TwoDAgent> Collection;

        double _d;
        Strombom2Base(int num, int length, double d, double eta, double radius): BaseModelR<Agent, Collection>(num, 1, length, eta, radius), _d(d)
        {
        }
        virtual Agent & updateAgentsDirection(int index) = 0;
        /* One time step of simulation */
        virtual Strombom2Base & oneTimeStep()
        {
            ++times;
            /* First, find the neighbors of every one */
            colle.setNeighbors();

            /* Iterate through all the agents */
            for(int i=0; i<_num; ++i)
            {
                colle.updateAgentsPosition(i);
                updateAgentsDirection(i);
                perturbateAgent(i);
            }
            return *this;
        }
};

class Strombom2Basic: public Strombom2Base
{
    public:
        double _c;

        Strombom2Basic(int num, int length, double d, double c, double eta, double radius): Strombom2Base(num, length, d, eta, radius), _c(c)
        {
        }

        virtual Agent & updateAgentsDirection(int index)
        {
            Agent & the_agent = colle[index];

            mylib::TwoDVector tvec(0, 0); // store the sum of all the position

            if(!the_agent.set_of_neighbors.empty())
            {
                for(int n: the_agent.set_of_neighbors)
                {
                    tvec.addBy(colle.mirror_of_agents[n].position);
                }
//                std::cout << tvec << std::endl;
                tvec.diveideBy(the_agent.set_of_neighbors.size()).subtractBy(the_agent.position).normalize().dot(_c);
            }

            the_agent.velocity.normalize().dot(_d).addBy(tvec);
            return the_agent;
        }

};

#endif
