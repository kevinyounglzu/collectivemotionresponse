#ifndef STROMBOM_H
#define STROMBOM_H

#include "collective.h"
#include "models.h"
#include "mymath.h"

/* Signal generators */
class SignalGenBase
{
    protected:
        double _dt;
    public:
        SignalGenBase(double dt): _dt(dt) {}
        virtual double gen(int times) = 0;
};

class ConstantValueGen: public SignalGenBase
{
    private:
        double _value;
    public:
        ConstantValueGen(double value):SignalGenBase(0), _value(value)
    {
    }

        virtual double gen(int times)
        {
            (void)times;
            return _value;
        }
};

/* External field */
/* Refactor needed! */
template <typename Agent>
class ExternalFieldBase
{
    protected:
        mylib::TwoDVector effect;
        double _dt;
    public:
        ExternalFieldBase(double dt): effect(), _dt(dt)
    {
    }

        virtual mylib::TwoDVector & gen(const Agent &, int times) = 0;
};

template <typename Agent>
class GhostNeighborBase
{
    protected:
        mylib::TwoDVector position;
    public:
        GhostNeighborBase(): position() {}
        virtual mylib::TwoDVector & getPosition(int times, const Agent & other) = 0;
};

class StrombomBase: public BaseModelR<collective::TwoDAgent, collective::CollectionPeriodicRBF<collective::TwoDAgent>>
{
    public:
        typedef collective::TwoDAgent Agent;
        typedef collective::CollectionPeriodicRBF<collective::TwoDAgent> Collection;

        double _d;

        StrombomBase(int num, int length, double d, double eta, double radius): BaseModelR<Agent, Collection>(num, 1, length, eta, radius), _d(d)
        {
        }
        virtual Agent & updateAgentsDirection(int index) = 0;
        /* One time step of simulation */
        virtual StrombomBase & oneTimeStep()
        {
            ++times;
            /* First, find the neighbors of every one */
            colle.setNeighbors();

            /* Iterate through all the agents */
            for(int i=0; i<_num; ++i)
            {
                colle.updateAgentsPosition(i);
                updateAgentsDirection(i);
                perturbateAgent(i);
            }
            return *this;
        }
};

class StrombomBasic: public StrombomBase
{
    public:
        double _c;
        StrombomBasic(int num, int length, double d, double c, double eta, double radius): StrombomBase(num, length, d, eta, radius), _c(c)
        {
        }

        virtual Agent & updateAgentsDirection(int index)
        {
            Agent & the_agent = colle[index];

            mylib::TwoDVector tvec(0, 0); // store the sum of all the position

//            mylib::printSet(the_agent.set_of_neighbors);

            if(!the_agent.set_of_neighbors.empty())
            {
                for(int n: the_agent.set_of_neighbors)
                {
//                    std::cout << current_colle[n].position;

                    tvec.addBy(colle.mirror_of_agents[n].position);
                }
//                std::cout << tvec << std::endl;
                tvec.diveideBy(the_agent.set_of_neighbors.size()).subtractBy(the_agent.position).normalize().dot(_c);
            }

            the_agent.velocity.normalize().dot(_d).addBy(tvec);
            return the_agent;
        }
};

class StrombomVariationC: public StrombomBase
{
    public:
        SignalGenBase & _signal_gen; // generate c signal
        StrombomVariationC(int num, int length, double d, SignalGenBase & signal_gen, double eta, double radius): StrombomBase(num, length, d, eta, radius), _signal_gen(signal_gen)
        {
        }

        virtual Agent & updateAgentsDirection(int index)
        {
            Agent & the_agent = colle[index];

            mylib::TwoDVector tvec(0, 0); // store the sum of all the position

//            mylib::printSet(the_agent.set_of_neighbors);

            if(!the_agent.set_of_neighbors.empty())
            {
                for(int n: the_agent.set_of_neighbors)
                {
//                    std::cout << current_colle[n].position;

                    tvec.addBy(colle.mirror_of_agents[n].position);
                }
//                std::cout << tvec << std::endl;
                tvec.diveideBy(the_agent.set_of_neighbors.size()).subtractBy(the_agent.position).normalize().dot(_signal_gen.gen(times));
            }

            the_agent.velocity.normalize().dot(_d).addBy(tvec);

            return the_agent;
        }
};

class StrombomExternalField: public StrombomBasic
{
    public:
        typedef collective::TwoDAgent Agent;

        ExternalFieldBase<Agent> & _external_field;

        StrombomExternalField(int num, int length, double d, double c,  ExternalFieldBase<Agent> & external_field, double eta, double radius): StrombomBasic(num, length, d, c, eta, radius), _external_field(external_field)
        {
        }

        virtual Agent & updateAgentsDirection(int index)
        {
            Agent & the_agent = colle[index];

            mylib::TwoDVector tvec(0, 0); // store the sum of all the position

//            mylib::printSet(the_agent.set_of_neighbors);

            if(!the_agent.set_of_neighbors.empty())
            {
                for(int n: the_agent.set_of_neighbors)
                {
//                    std::cout << current_colle[n].position;

                    tvec.addBy(colle.mirror_of_agents[n].position);
                }
//                std::cout << tvec << std::endl;
                tvec.diveideBy(the_agent.set_of_neighbors.size()).subtractBy(the_agent.position).normalize().dot(_c);
            }

            the_agent.velocity.normalize().dot(_d).addBy(tvec);
            the_agent.velocity.addBy(_external_field.gen(the_agent, times));

            return the_agent;
        }
};

class StrombomGhostNeighbor: public StrombomBasic
{
    public:
        typedef collective::TwoDAgent Agent;
        GhostNeighborBase<Agent> & _ghost_neighbor;
        StrombomGhostNeighbor(int num, int length, double d, double c, double eta, double radius, GhostNeighborBase<Agent> & ghost_neighbor): StrombomBasic(num, length, d, c, eta, radius), _ghost_neighbor(ghost_neighbor)
        {
        }

        virtual Agent & updateAgentsDirection(int index)
        {
            Agent & the_agent = colle[index];
            mylib::TwoDVector tvec(0, 0); // store the sum of all the position
            for(int n: the_agent.set_of_neighbors)
                tvec.addBy(colle.mirror_of_agents[n].position);
            tvec.addBy(_ghost_neighbor.getPosition(times, the_agent));
            tvec.diveideBy(the_agent.set_of_neighbors.size() + 1).subtractBy(the_agent.position).normalize().dot(_c);
            

            the_agent.velocity.normalize().dot(_d).addBy(tvec);
            return the_agent;
        }
};

#endif
