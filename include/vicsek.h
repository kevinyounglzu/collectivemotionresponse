#ifndef VICSEK_H
#define VICSEK_H

#include "collective.h"
#include "models.h"

/* Class for vicsek model with periodic boundry */
class VicsekBase: public BaseModelR<collective::TwoDAgent, collective::CollectionPeriodicRBF<collective::TwoDAgent>>
{
    public:
        typedef collective::TwoDAgent Agent;
        typedef collective::CollectionPeriodicRBF<Agent> Collection;

        VicsekBase(int num, double v, int length, double eta, double radius): BaseModelR<Agent, Collection>(num, v, length, eta, radius) {}

        /* Movie the agent and calculate the direction 
         * Depend on the colle.setNeighbors() */
        virtual Agent & updateAgentsDirection(int index) = 0;

        /* One time step of simulation */
        virtual VicsekBase & oneTimeStep()
        {
            /* First, find the neighbors of every one */
            colle.setNeighbors();

            /* Iterate through all the agents */
            for(int i=0; i<_num; ++i)
            {
                colle.updateAgentsPosition(i, _v);
                updateAgentsDirection(i);
                perturbateAgent(i);
            }
            return *this;
        }
};

class VicsekBasic: public VicsekBase
{
    public:
        VicsekBasic(int num, double v, int length, double eta, double radius): VicsekBase(num, v, length, eta, radius)
        {
        }

        virtual Agent & updateAgentsDirection(int index)
        {
            Agent & the_agent = colle[index];

            /* Calculate the direction by average the neighbors' */
            if(!the_agent.set_of_neighbors.empty())
            {
    //                cout << the_agent.velocity;
                for(int n : the_agent.set_of_neighbors)
                {
                    the_agent.velocity.addBy(colle.mirror_of_agents[n].velocity);
    //                    cout << the_agent.velocity;
                }
                the_agent.velocity.normalize();
            }
            return the_agent;
        }
};

#endif
