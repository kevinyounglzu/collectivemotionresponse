#ifndef COLLECTIVE_H
#define COLLECTIVE_H

#include <iostream>
#include <vector>
#include <set>
#include "mymath.h"
#include "rand.h"
#include <algorithm>

namespace collective
{
    struct TwoDAgent
    {
        public:
            mylib::TwoDVector position;
            mylib::TwoDVector velocity;
            std::set<int> set_of_neighbors; // to store the serial number of neighbors;

            TwoDAgent(): position(), velocity()
            {
            }
            TwoDAgent(double x, double y, double vx, double vy): position(x, y), velocity(vx, vy)
            {
            }

            TwoDAgent & operator=(const TwoDAgent & other)
            {
//                std::cout << "assignment" << std::endl;
                position = other.position;
                velocity = other.velocity;
                return *this;
            }

            friend std::ostream & operator<<(std::ostream & out, TwoDAgent & a)
            {
                out << "TwoDAgent@" << "<" << a.position.getX() << ", " << a.position.getY() << ">" << "V" << "<" << a.velocity.getX() << ", " << a.velocity.getY() << ">" << std::endl;
                return out;
            }
    };

    template <typename A> // A stands for TwoDAgent or so. A has to have default costructor
    class CollectionBase
    {
        protected:
            int _num; // numeber of agents;
            int _length; // the length of the plane
            std::vector<A> vec_of_agents; // vector to store all the agents;
        public:
            CollectionBase(int num, int length): _num(num), _length(length)
            {
                for(int i=0; i<_num; i++)
                    vec_of_agents.push_back(A());
            }
            /* Attrs methods */
            virtual int getNum() const { return _num; }
            virtual int getLength() const { return _length; }
            virtual A & operator[](const int index) { return vec_of_agents[index]; }

            /* Simulation methods */
            virtual CollectionBase & shufflePosition() // randomly distribute the agents
            {
                mylib::RandDouble rd(_length);
                for(auto & agent: vec_of_agents)
                {
                    agent.position.setX(rd.gen());
                    agent.position.setY(rd.gen());
                }
                return *this;
            }
            virtual CollectionBase & shuffleDirection() // randomly distribute the agents
            {
                mylib::RandDouble rd(-1, 1);
                for(auto & agent: vec_of_agents)
                {
                    agent.velocity.setX(rd.gen());
                    agent.velocity.setY(rd.gen());
                    agent.velocity.normalize();
                }
                return *this;
            }
            virtual bool isNeigborSetsEmpty() // check if all the agents' neighborsets are empty
            {
                return all_of(vec_of_agents.begin(), vec_of_agents.end(), [](A agent){return agent.set_of_neighbors.empty();});
            }
            virtual CollectionBase & clearAllNeighborSets() // clear all the agents' neighborsets
            {
                for(auto & agent: vec_of_agents)
                    agent.set_of_neighbors.clear();

                return *this;
            }

            /* Some abstract api */
            virtual CollectionBase & setNeighbors() = 0; // set neighbors for all the agents;
            virtual A & updateAgentsPosition(int index, double velocity_factor = 1.) = 0;
    };

    /* Collection with periodic boundry */
    template <typename A> // A stands for agents
    class CollectionPeriodic: public CollectionBase<A>
    {
        using CollectionBase<A>::_length;
        using CollectionBase<A>::vec_of_agents;

        public:
            CollectionPeriodic(int num, int length): CollectionBase<A>(num, length) {}
            
            /* implement the periodic boundry 
             * Assume that dx and dy is less than _length */
            virtual A & updateAgentsPosition(int index, double velocity_factor = 1.)
            {
                /* retrieve the agent */
                A & the_agent = vec_of_agents[index];

                /* Move the agents */
                double x = the_agent.position.getX() + velocity_factor * the_agent.velocity.getX();
                if(x > _length) x -= _length;
                if(x < 0) x += _length;
                the_agent.position.setX(x);

                double y = the_agent.position.getY() + velocity_factor * the_agent.velocity.getY();
                if(y > _length) y -= _length;
                if(y < 0) y += _length;
                the_agent.position.setY(y);

                return the_agent;
            }
    };

    /* Forehead declearation */
    template <typename A> 
    class AgentsMirror;

     /* Collection with peroidic boudnry and radius R using brute force and mirror techenique */
    template <typename A> // A stands for TwoDAgent or so.
    class CollectionPeriodicRBF: public CollectionPeriodic<A>
    {
        using CollectionBase<A>::clearAllNeighborSets;
        using CollectionBase<A>::vec_of_agents;
        using CollectionBase<A>::_length;
        using CollectionBase<A>::_num;

        protected:
            double _radius;
            A temp_agent;
        public:
            AgentsMirror<A> mirror_of_agents;

            CollectionPeriodicRBF(int num, int length, double radius): CollectionPeriodic<A>(num, length), _radius(radius), temp_agent(), mirror_of_agents(num) {}

            /* Attrs methods */
            virtual double getRadius() const { return _radius; }
            
            /* Simulation method */
            /* set the mirror vector and neighborsets of agents */
            virtual CollectionPeriodicRBF & setNeighbors()
            {
                /* set mirror */
                setMirrorOfAgents();
                setNeighborSets();
                /* set  neighborsets */
                return *this;
            }

            CollectionPeriodicRBF & setMirrorOfAgents()
            {
                mylib::TwoDVector tv;
                mirror_of_agents.clear();

                /* Copy the original agents to the mirror */
                for(const auto & agent: vec_of_agents)
                    mirror_of_agents.push_back(agent);

                /* Move the mirror agent to the mirror */
                for(const auto & agent: vec_of_agents)
                {
                    tv = agent.position;

                    /* cornors .... */
                    /* left buttom */
                    if(tv.getX() < _radius && tv.getY() < _radius)
                    {
                        temp_agent.velocity = agent.velocity;
                        temp_agent.position.setX(agent.position.getX() + _length);
                        temp_agent.position.setY(agent.position.getY() + _length);
                        mirror_of_agents.push_back(temp_agent);
                    }

                    /* left top */
                    if(tv.getX() < _radius && tv.getY() > (_length - _radius))
                    {
                        temp_agent.velocity = agent.velocity;
                        temp_agent.position.setX(agent.position.getX() + _length);
                        temp_agent.position.setY(agent.position.getY() - _length);
                        mirror_of_agents.push_back(temp_agent);
                    }

                    /* right buttom */
                    if(tv.getX() > (_length - _radius) && tv.getY() < _radius)
                    {
                        temp_agent.velocity = agent.velocity;
                        temp_agent.position.setX(agent.position.getX() - _length);
                        temp_agent.position.setY(agent.position.getY() + _length);
                        mirror_of_agents.push_back(temp_agent);
                    }

                    /* right top */
                    if(tv.getX() > (_length - _radius) && tv.getY() > (_length - _radius))
                    {
                        temp_agent.velocity = agent.velocity;
                        temp_agent.position.setX(agent.position.getX() - _length);
                        temp_agent.position.setY(agent.position.getY() - _length);
                        mirror_of_agents.push_back(temp_agent);
                    }

                    /* edges .. */
                    /* left */
                    if(tv.getX() < _radius)
                    {
                        temp_agent.velocity = agent.velocity;
                        temp_agent.position.setX(agent.position.getX() + _length);
                        temp_agent.position.setY(agent.position.getY());
                        mirror_of_agents.push_back(temp_agent);
                    }

                    /* buttom */
                    if(tv.getY() < _radius)
                    {
                        temp_agent.velocity = agent.velocity;
                        temp_agent.position.setX(agent.position.getX());
                        temp_agent.position.setY(agent.position.getY() + _length);
                        mirror_of_agents.push_back(temp_agent);
                    }

                    /* right */
                    if(tv.getX() > (_length - _radius))
                    {
                        temp_agent.velocity = agent.velocity;
                        temp_agent.position.setX(agent.position.getX() - _length);
                        temp_agent.position.setY(agent.position.getY());
                        mirror_of_agents.push_back(temp_agent);
                    }

                    /* top */
                    if(tv.getY() > (_length - _radius))
                    {
                        temp_agent.velocity = agent.velocity;
                        temp_agent.position.setX(agent.position.getX());
                        temp_agent.position.setY(agent.position.getY() - _length);
                        mirror_of_agents.push_back(temp_agent);
                    }
                }
                return *this;
            }

            /* Need to call setMirrorOfAgents before call this one,
             * usually use the setNeighbors method */
            CollectionPeriodicRBF & setNeighborSets()
            {
                clearAllNeighborSets();

                double r2 = _radius * _radius;

                /* deal with the agents inside the plane */
                for(int i=0; i<_num; ++i)
                {
                    for(int j=i+1; j<_num; ++j)
                    {
                        if(vec_of_agents[i].position.distanceSquare( mirror_of_agents[j].position) < r2)
                        {
                            vec_of_agents[i].set_of_neighbors.insert(j);
                            vec_of_agents[j].set_of_neighbors.insert(i);
                        }
                    }
                }

                /* deal with the agents on the mirror */
                for(int i=0; i<_num; ++i)
                {
                    for(int j=_num; j<mirror_of_agents.getSize(); ++j)
                    {
                        if(vec_of_agents[i].position.distanceSquare( mirror_of_agents[j].position) < r2)
                            vec_of_agents[i].set_of_neighbors.insert(j);
                    }
                }
                return *this;
            }
    };

    /* Helper class for CollectionPeriodicRBF */
    template <typename A> // the same as CollectionPeriodicRBF
    class AgentsMirror
    {
        private:
            int _num; // the number of the agents in the mirror
            int size; // the elements in mirror
            std::vector<A> mirror_vec_of_agents;

        public:
            AgentsMirror(int num): _num(num * 9), size(0)
            {
                /* the size of mirror should be 9 times of the original */
                for(int i=0; i<_num; ++i)
                    mirror_vec_of_agents.push_back(A());
            }

            /* Attrs methods */
            int getSize() const { return size; }
            int getCapacity() const { return _num; }
            A & operator[](const int index)
            {
                if(index >= size)
                {
                    std::cerr << "Mirror vector of agents out of boundry!" << std::endl;
                    throw("Mirror vector of agents out of boundry!");
                }
                return mirror_vec_of_agents[index];
            }
            A & push_back(const A & agent)
            {
                if(size == _num)
                {
                    std::cerr << "Mirror vector of agents out of capacity! " << std::endl;
                    return mirror_vec_of_agents[size - 1];
                }

                mirror_vec_of_agents[size].velocity = agent.velocity;
                mirror_vec_of_agents[size].position = agent.position;

                return mirror_vec_of_agents[size++];
            }
            AgentsMirror & clear()
            {
                size = 0;
                return *this;
            }
    };

    /* Collection with reflection boundry */
    template <typename A> // A stands for agents
    class CollectionReflection: public CollectionBase<A>
    {
        using CollectionBase<A>::_length;
        using CollectionBase<A>::vec_of_agents;

        public:
            CollectionReflection(int num, int length): CollectionBase<A>(num, length) {}
            
            /* implement the reflection boundry */
            virtual A & updateAgentsPosition(int index, double velocity_factor = 1.)
            {
                /* retrieve the agent */
                A & the_agent = vec_of_agents[index];

                /* Move the agents */
                double x = the_agent.position.getX() + velocity_factor * the_agent.velocity.getX();
                if(x > _length)
                {
                    x = _length * 2 - x;
                    the_agent.velocity.setX(-the_agent.velocity.getX());
                }
                if(x < 0)
                {
                    x = -x;
                    the_agent.velocity.setX(-the_agent.velocity.getX());
                }
                the_agent.position.setX(x);

                double y = the_agent.position.getY() + velocity_factor * the_agent.velocity.getY();
                if(y > _length)
                {
                    y = _length * 2 - y;
                    the_agent.velocity.setY(-the_agent.velocity.getY());
                }
                if(y < 0)
                {
                    y = -y;
                    the_agent.velocity.setY(-the_agent.velocity.getY());
                }
                the_agent.position.setY(y);

                return the_agent;
            }
    };

    /* Main goal: implement the setNeighbors method using brute force */
    template <typename A>
    class CollectionReflectionRBF: public CollectionReflection<A>
    {
        using CollectionBase<A>::clearAllNeighborSets;
        using CollectionBase<A>::_num;
        using CollectionBase<A>::vec_of_agents;

        protected:
            double _radius;
        public:
            std::vector<A> mirror_of_agents;

            CollectionReflectionRBF(int num, int length, double radius): CollectionReflection<A>(num, length), _radius(radius), mirror_of_agents()
            {
                for(int i=0; i<_num; ++i)
                    mirror_of_agents.push_back(A());

            }
            CollectionReflectionRBF & setNeighbors()
            {
                setMirrorOfAgents();
                setNeighborSets();
                return *this;
            }

            CollectionReflectionRBF & setMirrorOfAgents()
            {
                for(int i=0; i<_num; ++i)
                {
                    mirror_of_agents[i].position.setX(vec_of_agents[i].position.getX());
                    mirror_of_agents[i].position.setY(vec_of_agents[i].position.getY());
                    mirror_of_agents[i].velocity.setX(vec_of_agents[i].velocity.getX());
                    mirror_of_agents[i].velocity.setY(vec_of_agents[i].velocity.getY());
                }
                return *this;
            }

            CollectionReflectionRBF & setNeighborSets()
            {
                /* should clear all the neighbor sets before modification */
                clearAllNeighborSets();

                double r2 = _radius * _radius;

                for(int i=0; i<_num; ++i)
                {
                    for(int j=i+1; j<_num; ++j)
                    {
                        if(vec_of_agents[i].position.distanceSquare(vec_of_agents[j].position) < r2)
                        {
                            vec_of_agents[i].set_of_neighbors.insert(j);
                            vec_of_agents[j].set_of_neighbors.insert(i);
                        }
                    }
                }
                return *this;
            }
    };

} // end of namespace collective

#endif
