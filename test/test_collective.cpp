#include <gtest/gtest.h>
#include "collective.h"
#include "mymath.h"
#include <iostream>
#include "utils.h"

using namespace collective;
using namespace std;

TEST(Test_Agent, initialization)
{
    TwoDAgent a;
    EXPECT_DOUBLE_EQ(a.position.getX(), 0);
    EXPECT_DOUBLE_EQ(a.position.getY(), 0);
    EXPECT_DOUBLE_EQ(a.velocity.getX(), 0);
    EXPECT_DOUBLE_EQ(a.velocity.getY(), 0);

    TwoDAgent b(10., 2., 3., 5.);
    EXPECT_DOUBLE_EQ(b.position.getX(), 10.);
    EXPECT_DOUBLE_EQ(b.position.getY(), 2.);
    EXPECT_DOUBLE_EQ(b.velocity.getX(), 3.);
    EXPECT_DOUBLE_EQ(b.velocity.getY(), 5.);
}

TEST(Test_Agent, Test_copy_constructor)
{
    TwoDAgent a;
    TwoDAgent b(10., 2., 3., 5.);
    a = b;
    EXPECT_DOUBLE_EQ(a.position.getX(), 10.);
    EXPECT_DOUBLE_EQ(a.position.getY(), 2.);
    EXPECT_DOUBLE_EQ(a.velocity.getX(), 3.);
    EXPECT_DOUBLE_EQ(a.velocity.getY(), 5.);
}

/* Test for CollecitonBase class */
class CollectionBaseTest: public CollectionBase<TwoDAgent>
{
    public:
        CollectionBaseTest(int n, int l): CollectionBase<TwoDAgent>(n, l){}
        virtual CollectionBase & setNeighbors() { return *this; }
        virtual TwoDAgent & updateAgentsPosition(int index, double velocity_factor = 1.)
        {
            ++velocity_factor;
            return vec_of_agents[index];
        }
};

TEST(Test_CollectiveBase, initialization)
{
    int n(10), l(11);

    CollectionBaseTest colle(n, l);
    EXPECT_EQ(colle.getNum(), n);
    EXPECT_EQ(colle.getLength(), l);

    for(int i=0; i<colle.getNum(); i++)
    {
        EXPECT_DOUBLE_EQ(colle[i].position.getX(), 0);
        EXPECT_DOUBLE_EQ(colle[i].position.getY(), 0);
        EXPECT_DOUBLE_EQ(colle[i].velocity.getX(), 0);
        EXPECT_DOUBLE_EQ(colle[i].velocity.getY(), 0);
    }
}

TEST(Test_CollectiveBase, Test_setting)
{
    int n(100);
    CollectionBaseTest colle(n, 10);

    for(int i=0; i<colle.getNum(); i++)
    {
        colle[i].position.setX(static_cast<double>(i));
        colle[i].position.setY(static_cast<double>(i*i));
    }

    for(int i=0; i<colle.getNum(); i++)
    {
        EXPECT_DOUBLE_EQ(colle[i].position.getX(), static_cast<double>(i));
        EXPECT_DOUBLE_EQ(colle[i].position.getY(), static_cast<double>(i*i));
    }
}

TEST(Test_CollectiveBase, Test_shuufle)
{
    int n(10), l(100);
    CollectionBaseTest colle(n, l);

    colle.shufflePosition();
    for(int i=0; i<n; i++)
    {
//        cout << colle[i];
        EXPECT_GE(colle[i].position.getX(), 0);
        EXPECT_LT(colle[i].position.getX(), l);
    }

    colle.shuffleDirection();
    for(int i=0; i<n; ++i)
    {
//        cout << colle[i].velocity <<endl;
        double x = colle[i].velocity.getX();
        double y = colle[i].velocity.getY();
        EXPECT_DOUBLE_EQ(x*x + y*y, 1.);
    }
}

TEST(Test_Collective, Test_is_neighbor_empty_clear)
{
    int n(10), l(10);
    CollectionBaseTest colle(n, l);

    colle.shufflePosition();
    EXPECT_TRUE(colle.isNeigborSetsEmpty());

    colle[0].set_of_neighbors.insert(10);
    EXPECT_FALSE(colle.isNeigborSetsEmpty());

    colle.clearAllNeighborSets();
    EXPECT_TRUE(colle.isNeigborSetsEmpty());

    for(int i=0; i<n; ++i)
        colle[i].set_of_neighbors.insert(i);
    EXPECT_FALSE(colle.isNeigborSetsEmpty());

    colle.clearAllNeighborSets();
    EXPECT_TRUE(colle.isNeigborSetsEmpty());
}

/* Tests for CollectionPeriodic */
class CollectionPeriodicTest: public CollectionPeriodic<TwoDAgent>
{
    public:
        CollectionPeriodicTest(int n, int l):CollectionPeriodic<TwoDAgent>(n, l) {}
        virtual CollectionBase & setNeighbors() { return *this; }
};

TEST(Test_CollectionPeriodic, Test_update_agents_position)
{
    int n(1), l(2);
    double percision(1e-6);
    CollectionPeriodicTest cp(n, l);

    /* Test for basic update */
    cp[0].position.setX(0.);
    cp[0].position.setY(0.);
    cp[0].velocity.setX(1.);
    cp[0].velocity.setY(1.5);

    cp.updateAgentsPosition(0);
    EXPECT_DOUBLE_EQ(cp[0].position.getX(), 1.);
    EXPECT_DOUBLE_EQ(cp[0].position.getY(), 1.5);

    /* Test for periodic boundry */
    cp[0].position.setX(1.);
    cp[0].position.setY(1.);
    cp[0].velocity.setX(1.2);
    cp[0].velocity.setY(1.5);

    cp.updateAgentsPosition(0);
    EXPECT_NEAR(cp[0].position.getX(), 0.2, percision);
    EXPECT_NEAR(cp[0].position.getY(), 0.5, percision);

    /* Test for periodic boundry */
    cp[0].position.setX(1.);
    cp[0].position.setY(1.);
    cp[0].velocity.setX(-1.2);
    cp[0].velocity.setY(-1.5);

    cp.updateAgentsPosition(0);
    EXPECT_NEAR(cp[0].position.getX(), 1.8, percision);
    EXPECT_NEAR(cp[0].position.getY(), 1.5, percision);

    /* Test for update with factor */
    cp[0].position.setX(0.);
    cp[0].position.setY(0.);
    cp[0].velocity.setX(1.3);
    cp[0].velocity.setY(1.5);

    cp.updateAgentsPosition(0, 2);
    EXPECT_NEAR(cp[0].position.getX(), 0.6, percision);
    EXPECT_NEAR(cp[0].position.getY(), 1., percision);
}

/* Tests for AgentsMirror */
TEST(Test_AgentsMirror, Test_Initialization)
{
    int n(10);
    AgentsMirror<TwoDAgent> am(n);
    EXPECT_EQ(am.getSize(), 0);
    EXPECT_EQ(am.getCapacity(), n*9);
}

TEST(Test_AgentsMirror, Test_push_back_clear)
{
    int n(20);
    AgentsMirror<TwoDAgent> am(n);
    double x(10.), y(20.), vx(4.), vy(-2.);
    TwoDAgent ta(x, y, vx, vy);

    am.push_back(ta);
    EXPECT_EQ(am.getSize(), 1);
    EXPECT_EQ(am[0].position.getX(), x);
    EXPECT_EQ(am[0].position.getY(), y);
    EXPECT_EQ(am[0].velocity.getX(), vx);
    EXPECT_EQ(am[0].velocity.getY(), vy);

    am.push_back(ta);
    EXPECT_EQ(am.getSize(), 2);
    EXPECT_EQ(am[1].position.getX(), x);
    EXPECT_EQ(am[1].position.getY(), y);
    EXPECT_EQ(am[1].velocity.getX(), vx);
    EXPECT_EQ(am[1].velocity.getY(), vy);

    am.clear();
    EXPECT_EQ(am.getSize(), 0);
}

TEST(Test_AgentsMirror, Test_Setting)
{
    int n(10);
    AgentsMirror<TwoDAgent> am(n);
    TwoDAgent td(0, 0, 0, 0);

    for(int i=0; i<am.getCapacity(); ++i)
    {
        td.velocity.setX(i);
        td.velocity.setY(i*2);
        td.position.setX(i*3);
        td.position.setY(i*4);

        am.push_back(td);
    }
    for(int i=0; i<am.getCapacity(); ++i)
    {
        EXPECT_DOUBLE_EQ(am[i].velocity.getX(), i);
        EXPECT_DOUBLE_EQ(am[i].velocity.getY(), i*2);
        EXPECT_DOUBLE_EQ(am[i].position.getX(), i*3);
        EXPECT_DOUBLE_EQ(am[i].position.getY(), i*4);
    }
}

/* Tests for CollecitonPeriodicR */
TEST(Test_CollectionPeriodicRBF, Test_Initialization)
{
    int n(10), l(10);
    double r(2.);
    CollectionPeriodicRBF<TwoDAgent> cpr(n, l, r);
    EXPECT_DOUBLE_EQ(cpr.getRadius(), r);
    EXPECT_EQ(cpr.getNum(), n);
    EXPECT_EQ(cpr.getLength(), l);
}

TEST(Test_CollectionPeriodicRBF, Test_set_mirror_of_agents)
{
    int n(1), l(10);
    double r(2.0);

    /* The tests depend on the order of the implementation */

    CollectionPeriodicRBF<TwoDAgent> crbf(n, l, r);
    EXPECT_EQ(crbf.mirror_of_agents.getSize(), 0);

    /* Without mirror agent */
    crbf[0].position.setX(5.);
    crbf[0].position.setY(5.);
    crbf[0].velocity.setX(2.);
    crbf[0].velocity.setY(4.);

    crbf.setMirrorOfAgents();
    EXPECT_EQ(crbf.mirror_of_agents.getSize(), 1);

    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[0].position.getX(), 5.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[0].position.getY(), 5.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[0].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[0].velocity.getY(), 4.);

    crbf.mirror_of_agents.clear();
    /* left buttom */
    crbf[0].position.setX(1.);
    crbf[0].position.setY(1.);
    crbf[0].velocity.setX(2.);
    crbf[0].velocity.setY(4.);
    crbf.setMirrorOfAgents();
    EXPECT_EQ(crbf.mirror_of_agents.getSize(), 4);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getX(), 11.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getY(), 11.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getY(), 4.);

    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].position.getX(), 11.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].position.getY(), 1.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].velocity.getY(), 4.);

    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].position.getX(), 1.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].position.getY(), 11.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].velocity.getY(), 4.);


    crbf.mirror_of_agents.clear();
    /* right buttom */
    crbf[0].position.setX(9.5);
    crbf[0].position.setY(1.);
    crbf.setMirrorOfAgents();
    EXPECT_EQ(crbf.mirror_of_agents.getSize(), 4);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getX(), -0.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getY(), 11.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getY(), 4.);

    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].position.getX(), 9.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].position.getY(), 11.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].velocity.getY(), 4.);

    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].position.getX(), -0.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].position.getY(), 1.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].velocity.getY(), 4.);


    crbf.mirror_of_agents.clear();
    /* right top */
    crbf[0].position.setX(9.0);
    crbf[0].position.setY(9.0);
    crbf.setMirrorOfAgents();
    EXPECT_EQ(crbf.mirror_of_agents.getSize(), 4);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getX(), -1.0);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getY(), -1.0);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getY(), 4.);

    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].position.getX(), -1.0);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].position.getY(), 9.0);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].velocity.getY(), 4.);

    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].position.getX(), 9.0);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].position.getY(), -1.0);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].velocity.getY(), 4.);


    crbf.mirror_of_agents.clear();
    /* left top */
    crbf[0].position.setX(1.5);
    crbf[0].position.setY(9.5);
    crbf.setMirrorOfAgents();
    EXPECT_EQ(crbf.mirror_of_agents.getSize(), 4);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getX(), 11.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getY(), -0.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getY(), 4.);

    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].position.getX(), 11.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].position.getY(), 9.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[2].velocity.getY(), 4.);

    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].position.getX(), 1.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].position.getY(), -0.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[3].velocity.getY(), 4.);


    crbf.mirror_of_agents.clear();
    /* left */
    crbf[0].position.setX(1.5);
    crbf[0].position.setY(4.);
    crbf.setMirrorOfAgents();
    EXPECT_EQ(crbf.mirror_of_agents.getSize(), 2);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getX(), 11.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getY(), 4.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getY(), 4.);


    crbf.mirror_of_agents.clear();
    /* buttom */
    crbf[0].position.setX(6.);
    crbf[0].position.setY(1.);
    crbf.setMirrorOfAgents();
    EXPECT_EQ(crbf.mirror_of_agents.getSize(), 2);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getX(), 6.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getY(), 11.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getY(), 4.);


    crbf.mirror_of_agents.clear();
    /* right */
    crbf[0].position.setX(8.5);
    crbf[0].position.setY(5.);
    crbf.setMirrorOfAgents();
    EXPECT_EQ(crbf.mirror_of_agents.getSize(), 2);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getX(), -1.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getY(), 5.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getY(), 4.);


    crbf.mirror_of_agents.clear();
    /* top */
    crbf[0].position.setX(7.);
    crbf[0].position.setY(9.5);
    crbf.setMirrorOfAgents();
    EXPECT_EQ(crbf.mirror_of_agents.getSize(), 2);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getX(), 7.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].position.getY(), -0.5);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getX(), 2.);
    EXPECT_DOUBLE_EQ(crbf.mirror_of_agents[1].velocity.getY(), 4.);


    /* Tests for multiagents */
    n = 3;
    CollectionPeriodicRBF<TwoDAgent> crbf2(n, l, r);

    crbf2[0].position.setX(1.);
    crbf2[0].position.setY(1.);
    crbf2[0].velocity.setX(2.);
    crbf2[0].velocity.setY(4.);

    crbf2[1].position.setX(3.);
    crbf2[1].position.setY(9.5);
    crbf2[1].velocity.setX(4.);
    crbf2[1].velocity.setY(5.);

    crbf2[2].position.setX(3.);
    crbf2[2].position.setY(4.);
    crbf2[2].velocity.setX(5.);
    crbf2[2].velocity.setY(6.);

    crbf2.setMirrorOfAgents();
    EXPECT_EQ(crbf2.mirror_of_agents.getSize(), 7);

//    for(int i=0; i<crbf2.mirror_of_agents.getSize(); ++i)
//    {
//        cout << crbf2.mirror_of_agents[i] ;
//    }
}

TEST(Test_CollectionPeriodicRBF, Test_set_neighbor_sets)
{
    int n(100), l(10);
    double r(2.0);

    CollectionPeriodicRBF<TwoDAgent> crbf(n, l, r);
    crbf.shufflePosition();
    crbf.setNeighbors();

    for(int i=0; i<crbf.getNum(); ++i)
    {
        for(int index: crbf[i].set_of_neighbors)
        {
            EXPECT_TRUE(crbf[i].position.distance(crbf.mirror_of_agents[index].position) < crbf.getRadius());
        }
    }

    /* Tests for existence 
     * The tests depend on the order of the implementation */
    n = 2;
    CollectionPeriodicRBF<TwoDAgent> crbf2(n, l, r);

    crbf2[0].position.setX(4.5);
    crbf2[0].position.setY(6.5);
    crbf2[1].position.setX(5.5);
    crbf2[1].position.setY(5.);
    crbf2.setNeighbors();
    EXPECT_EQ(crbf2[0].set_of_neighbors.size(), 1);
    EXPECT_EQ(crbf2[1].set_of_neighbors.size(), 1);
    EXPECT_TRUE(crbf2[0].set_of_neighbors.find(1) != crbf2[0].set_of_neighbors.end());
    EXPECT_TRUE(crbf2[1].set_of_neighbors.find(0) != crbf2[1].set_of_neighbors.end());

    crbf2[0].position.setX(0.5);
    crbf2[0].position.setY(0.5);
    crbf2[1].position.setX(9.5);
    crbf2[1].position.setY(9.5);
    crbf2.setNeighbors();
    EXPECT_EQ(crbf2[0].set_of_neighbors.size(), 1);
    EXPECT_EQ(crbf2[1].set_of_neighbors.size(), 1);
    EXPECT_TRUE(crbf2[0].set_of_neighbors.find(5) != crbf2[0].set_of_neighbors.end());
    EXPECT_TRUE(crbf2[1].set_of_neighbors.find(2) != crbf2[1].set_of_neighbors.end());
}

/* Tests for CollectionReflection */
class CollectionReflectionTest: public CollectionReflection<TwoDAgent>
{
    public:
        CollectionReflectionTest(int n, int l):CollectionReflection<TwoDAgent>(n, l) {}
        virtual CollectionBase & setNeighbors() { return *this; }
};

TEST(Test_CollectionReflection, Test_update_agents_position)
{
    int n(1), l(2);
    CollectionReflectionTest cr(n, l);

    cr[0].position.setX(0.);
    cr[0].position.setY(0.);
    cr[0].velocity.setX(1.);
    cr[0].velocity.setY(1.5);

    cr.updateAgentsPosition(0);
    EXPECT_DOUBLE_EQ(cr[0].position.getX(), 1.);
    EXPECT_DOUBLE_EQ(cr[0].position.getY(), 1.5);
    EXPECT_DOUBLE_EQ(cr[0].velocity.getX(), 1.);
    EXPECT_DOUBLE_EQ(cr[0].velocity.getY(), 1.5);

    /* Test for reflection boundry */
    cr[0].position.setX(1.);
    cr[0].position.setY(1.);
    cr[0].velocity.setX(1.2);
    cr[0].velocity.setY(0);

    cr.updateAgentsPosition(0);
    EXPECT_NEAR(cr[0].position.getX(), 1.8, 0.000001);
    EXPECT_DOUBLE_EQ(cr[0].velocity.getX(), -1.2);

    cr[0].velocity.setY(1.8);
    cr.updateAgentsPosition(0);
    EXPECT_NEAR(cr[0].position.getY(), 1.2, 0.000001);
    EXPECT_DOUBLE_EQ(cr[0].velocity.getY(), -1.8);

    /* Test for reflection boundry */
    cr[0].position.setX(1.);
    cr[0].position.setY(1.);
    cr[0].velocity.setX(-1.2);
    cr[0].velocity.setY(-1.5);

    cr.updateAgentsPosition(0);
    EXPECT_NEAR(cr[0].position.getX(), 0.2, 0.000001);
    EXPECT_NEAR(cr[0].position.getY(), 0.5, 0.000001);
    EXPECT_DOUBLE_EQ(cr[0].velocity.getX(), 1.2);
    EXPECT_DOUBLE_EQ(cr[0].velocity.getY(), 1.5);
}

TEST(Test_CollectionReflectionRBF, Test_set_mirror_of_agents)
{
    int n(100), l(10);
    double r(2.);
    CollectionReflectionRBF<TwoDAgent> cr(n, l, r);

    EXPECT_EQ(cr.mirror_of_agents.size(), n);

    cr.shufflePosition();
    cr.shuffleDirection();

    cr.setMirrorOfAgents();

    for(int i=0; i<n; ++i)
    {
        EXPECT_EQ(cr[i].position.getX(), cr.mirror_of_agents[i].position.getX());
        EXPECT_EQ(cr[i].position.getY(), cr.mirror_of_agents[i].position.getY());
        EXPECT_EQ(cr[i].velocity.getX(), cr.mirror_of_agents[i].velocity.getX());
        EXPECT_EQ(cr[i].velocity.getY(), cr.mirror_of_agents[i].velocity.getY());
    }
}

TEST(Test_CollectionReflectionRBF, Test_set_neighbors)
{
    int n(3), l(10);
    double r(2.);
    CollectionReflectionRBF<TwoDAgent> cr(n, l, r);

    cr[0].position.setX(0.);
    cr[0].position.setY(0.);
    cr[1].position.setX(1.);
    cr[1].position.setY(1.5);
    cr[2].position.setX(2.);
    cr[2].position.setY(1.5);

    cr.setNeighbors();
    EXPECT_EQ(cr[0].set_of_neighbors.size(), 1);
    EXPECT_NE(cr[0].set_of_neighbors.find(1), cr[0].set_of_neighbors.end());

    EXPECT_EQ(cr[1].set_of_neighbors.size(), 2);
    EXPECT_NE(cr[1].set_of_neighbors.find(0), cr[1].set_of_neighbors.end());
    EXPECT_NE(cr[1].set_of_neighbors.find(2), cr[1].set_of_neighbors.end());

    EXPECT_EQ(cr[2].set_of_neighbors.size(), 1);
    EXPECT_NE(cr[2].set_of_neighbors.find(1), cr[2].set_of_neighbors.end());

    n = 100;
    CollectionReflectionRBF<TwoDAgent> cr1(n, l, r);
    cr1.shufflePosition();
    cr1.setNeighbors();
    
    for(int i=0; i<n; ++i)
    {
        for(int j=i+1; j<n; ++j)
        {
            if(cr1[i].position.distance(cr1[j].position) < r)
            {
                EXPECT_TRUE(cr1[i].set_of_neighbors.find(j) != cr1[i].set_of_neighbors.end());
                EXPECT_TRUE(cr1[j].set_of_neighbors.find(i) != cr1[j].set_of_neighbors.end());
            }
            else
            {
                EXPECT_TRUE(cr1[i].set_of_neighbors.find(j) == cr1[i].set_of_neighbors.end());
                EXPECT_TRUE(cr1[j].set_of_neighbors.find(i) == cr1[j].set_of_neighbors.end());
            }
        }
    }
}
