#include <gtest/gtest.h>
#include "collective.h"
#include "models.h"
#include "utils.h"
#include <cmath>

/* instance the BaseModel class */
class BaseModelTest: public BaseModelR<collective::TwoDAgent, collective::CollectionPeriodicRBF<collective::TwoDAgent>>
{
    public:
        typedef collective::TwoDAgent Agent;
        BaseModelTest(int num, double v, int length, double eta, double radius): BaseModelR(num, v, length, eta, radius) {}

        virtual Agent & updateAgentsDirection(int index) { return colle[index]; }
        virtual BaseModelTest & oneTimeStep() { return *this; }
};

TEST(Test_BaseModelR, Test_Initialization)
{
    int n(10), l(10);
    double r(6.), v(0.2), eta(0.5);
    BaseModelTest bt(n, v, l, eta, r);

    EXPECT_EQ(bt._num, n);
    EXPECT_EQ(bt._length, l);
    EXPECT_DOUBLE_EQ(bt._eta, eta);
    EXPECT_DOUBLE_EQ(bt.colle.getRadius(), r);
    
    for(int i=0; i<n; ++i)
    {
        BaseModelTest::Agent & agent = bt.colle[i];
        EXPECT_GE(agent.position.getX(), 0.);
        EXPECT_GE(agent.position.getY(), 0.);
        EXPECT_GE(agent.velocity.getX(), -1.);
        EXPECT_GE(agent.velocity.getY(), -1.);
    }
}

/* Rethink about how to test this needed */
//TEST(Test_Vicsek, Test_update_agent_direction)
//{
//    int n(2), l(2);
//    double r(4.), v(0.2), eta(0.5);
//    Vicsek vic(n, v, l, eta, r);
//    vic.colle[0].velocity.setX(0.);
//    vic.colle[0].velocity.setY(0.);
//
//    vic.colle.setNeighbors();
//    vic.updateAgentsDirection(0);
//
//    EXPECT_DOUBLE_EQ(vic.colle[0].velocity.getX(), vic.colle[1].velocity.getX());
//    EXPECT_DOUBLE_EQ(vic.colle[0].velocity.getY(), vic.colle[1].velocity.getY());
//
//    Vicsek vic1(3, v, l, eta, r);
//    vic1.colle[0].velocity.setX(0.);
//    vic1.colle[0].velocity.setY(0.);
//
//    vic1.colle.setNeighbors();
//    vic1.updateAgentsDirection(0);
//
//    double x1 = vic1.colle.mirror_of_agents[1].velocity.getX();
//    double x2 = vic1.colle.mirror_of_agents[2].velocity.getX();
//    double y1 = vic1.colle.mirror_of_agents[1].velocity.getY();
//    double y2 = vic1.colle.mirror_of_agents[2].velocity.getY();
//    double x = x1 + x2;
//    double y = y1 + y2;
//    double base = sqrt(x * x + y * y);
//
//    EXPECT_DOUBLE_EQ(vic1.colle[0].velocity.getX(), x / base);
//    EXPECT_DOUBLE_EQ(vic1.colle[0].velocity.getY(), y / base);
//}

//TEST(Test_Vicsek, Test_perturbate)
//{
//    int n(2), l(2);
//    double r(4.), v(0.2), eta(0.5);
//    Vicsek vic(n, v, l, eta, r);
//    
//    cout << vic.current_colle[0].velocity;
//    vic.perturbateAgent(0);
//    cout << vic.current_colle[0].velocity;
//}
