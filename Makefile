CPPFLAGS=-Iinclude -std=c++11 -g -Wall -Wextra

MYLIB_PATH=/Users/kevinyoung/working/mycpplib
MYLIB_I=$(MYLIB_PATH)/include
MYLIB_L=$(MYLIB_PATH)/lib
MYLIB=mycpp
MYLIB_FLAG=-I$(MYLIB_I) -L$(MYLIB_L) -l$(MYLIB)

GTEST=gtest

TEST_SRC=$(wildcard test/*.cpp)
TEST_OBJ=$(patsubst %.cpp,%.o,$(TEST_SRC))
TEST=test/test
.PONY: tests clean run cleandata cleanall


tests: $(TEST)
	./$(TEST)

$(TEST): CPPFLAGS+=-I$(MYLIB_I)
$(TEST): $(TEST_OBJ) $(HEADERS)
	$(CXX) $(CPPFLAGS) $(MYLIB_FLAG) -l$(GTEST) $(TEST_OBJ) -o $@

RUN_SRC=src/vicsek.cpp
RUN=$(patsubst %.cpp,%,$(RUN_SRC))
RUN_OBJ=$(patsubst %.cpp,%.o,$(RUN_SRC))

run: CPPFLAGS+=$(MYLIB_FLAG) -O2
run: $(RUN)
	./$(RUN)

$(RUN): $(RUN_SRC)
	mkdir -p data
	$(CXX) $(CPPFLAGS) $< -o $@

clean:
	rm -rf $(TEST) $(TEST_OBJ)
	rm -rf $(RUN) $(RUN_OBJ)
	rm -rf `find . -name "*.dSYM" -print`

cleanall: clean
	rm -rf data/*

cleandata:
	rm -rf data/*
