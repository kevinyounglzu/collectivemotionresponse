#include "vicsek.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include "parser.h"
#include <cmath>
#include <string>

using namespace collective;
using namespace std;
using namespace mylib;

int main()
{
    Parser ps("./config/vicsek.cfg");
    int n = ps.getInt("n");
    int l = ps.getInt("l");
    int times = ps.getInt("times");
    double r = ps.getDouble("r");
    double v = ps.getDouble("v");
    double eta = ps.getDouble("eta");

//    cout << n << endl;
//    cout << l << endl;
//    cout << r << endl;
//    cout << v << endl;
//    cout << eta << endl;


//    string filename_base = "/Users/kevinyoung/working/academy/response/data/";
    string filename_base = "./data/";
    string file_suffix = ".out";
    ostringstream str;
    ofstream out;

    VicsekBasic vic(n, v, l, eta, r);


    for(int i=0; i<times; ++i)
    {
        cout << i << endl;
        str << filename_base << i << file_suffix;

        out.open(str.str());

        vic.oneTimeStep();
        vic.dumpPosition(out);

        out.close();
        str.str(string());
    }

    return 0;
}
