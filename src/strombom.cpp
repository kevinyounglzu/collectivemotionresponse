#include "strombom.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include "parser.h"
#include <cmath>
#include <string>

using namespace collective;
using namespace std;
using namespace mylib;

class CosValueGen: public SignalGenBase
{
    protected:
        double _amp;
        double _base;
    public:
        CosValueGen(double dt, double amp, double base): SignalGenBase(dt), _amp(amp), _base(base)
        {
        }
        virtual double gen(int times)
        {
            return cos(_dt * times) * _amp + _base;
        }
};

class ConstantField: public ExternalFieldBase<TwoDAgent>
{
    protected:
    public:
        ConstantField(): ExternalFieldBase<TwoDAgent>(0)
        {}
        virtual TwoDVector & gen(const TwoDAgent & agent, int times)
        {
            (void)agent;
            (void)times;
            return effect;
        }

};

class CosField: public ExternalFieldBase<TwoDAgent>
{
    protected:
        double _amp;
    public:
        CosField(double dt, double amp): ExternalFieldBase<TwoDAgent>(dt),  _amp(amp) {}
        virtual TwoDVector & gen(const TwoDAgent & agent, int times)
        {
            (void)agent;
            effect.setY(0);
            effect.setX(_amp * cos(_dt * times));
            return effect;
        }
};

class CosGhostNeighbor: public GhostNeighborBase<TwoDAgent>
{
    private:
        double _radius;
        double _dt;
    public:
        CosGhostNeighbor(double radius, double dt): GhostNeighborBase<TwoDAgent>(), _radius(radius), _dt(dt)
        {
        }
        virtual TwoDVector & getPosition(int times, const TwoDAgent & other)
        {
            position.setY(other.position.getY());
            position.setX(other.position.getX() + _radius * cos(_dt * times));
            return position;
        }

};

int main()
{
    Parser ps("./config/strombom.cfg");

    int n = ps.getInt("n");
    int l = ps.getInt("l");
    int times = ps.getInt("times");
    double radius = ps.getDouble("radius");
    double d = ps.getDouble("d");
    double c = ps.getDouble("c");
    double eta = ps.getDouble("eta");
    double amp = ps.getDouble("amp");
    double dt = ps.getDouble("dt");
//    string filename_base = "/Users/kevinyoung/working/academy/response/data/";
    string filename_base = "./data/";
    string file_suffix = ".out";
    ostringstream str;
    ofstream out;


    /* ******************************************************** */
    /* basic */
//    StrombomBasic sb(n, l, d, c, eta, radius);

    /* ******************************************************** */
    /* variant c */
//    ConstantValueGen cg(c);
//    CosValueGen cosvg(dt, 0.25, 0.3);
//    StrombomVariationC sb(n, l, d, cosvg, eta, radius);

    /* ******************************************************** */
    /* External field */
//    ConstantField constf;
//    CosField cosf(dt, amp);
//    StrombomExternalField sb(n, l, d, c, cosf, eta, radius);

    /* ******************************************************** */
    /* Ghost neighbor */
    CosGhostNeighbor cosghostn(radius, dt);
    StrombomGhostNeighbor sb(n, l, d, c, eta, radius, cosghostn);


    cout << 0 << endl;
    str << filename_base << 0 << file_suffix;
    out.open(str.str());
    sb.dumpPosition(out);

    for(int i=1; i<times; ++i)
    {
        cout << i << endl;
        str << filename_base << i << file_suffix;

        out.open(str.str());

        sb.oneTimeStep();
        sb.dumpPosition(out);

        out.close();
        str.str(string());
    }

    return 0;
}
