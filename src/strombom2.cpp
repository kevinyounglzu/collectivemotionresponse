#include "strombom2.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include "parser.h"
#include <cmath>
#include <string>

using namespace collective;
using namespace std;
using namespace mylib;

int main()
{
    Parser ps("./config/strombom.cfg");

    int n = ps.getInt("n");
    int l = ps.getInt("l");
    int times = ps.getInt("times");
    double radius = ps.getDouble("radius");
    double d = ps.getDouble("d");
    double c = ps.getDouble("c");
    double eta = ps.getDouble("eta");
    double amp = ps.getDouble("amp");
    double dt = ps.getDouble("dt");
//    string filename_base = "/Users/kevinyoung/working/academy/response/data/";
    string filename_base = "./data/";
    string file_suffix = ".out";
    ostringstream str;
    ofstream out;


    /* ******************************************************** */
    /* basic */
    Strombom2Basic sb(n, l, d, c, eta, radius);

    /* ******************************************************** */
    /* variant c */
//    ConstantValueGen cg(c);
//    CosValueGen cosvg(dt, 0.25, 0.3);
//    StrombomVariationC sb(n, l, d, cosvg, eta, radius);

    /* ******************************************************** */
    /* External field */
//    ConstantField constf;
//    CosField cosf(dt, amp);
//    StrombomExternalField sb(n, l, d, c, cosf, eta, radius);

    /* ******************************************************** */
    /* Ghost neighbor */
//    CosGhostNeighbor cosghostn(radius, dt);
//    StrombomGhostNeighbor sb(n, l, d, c, eta, radius, cosghostn);


    cout << 0 << endl;
    str << filename_base << 0 << file_suffix;
    out.open(str.str());
    sb.dumpPosition(out);

    for(int i=1; i<times; ++i)
    {
        cout << i << endl;
        str << filename_base << i << file_suffix;

        out.open(str.str());

        sb.oneTimeStep();
        sb.dumpPosition(out);

        out.close();
        str.str(string());
    }

    return 0;
}
